function group(data, n) {
	data = Array.from(data)
	let grps = []
	for (let i = 0; i < n; i++) {
		grps.push([])
		grps[i].isSorted = false
	}
	grps[0].push(data.shift())
	grps[grps.length - 1].push(data.pop())

	grps.min = function (i) {
		if (this[i].isSorted) {
			return this[i][0]
		}
		return Math.min.apply(null, this[i])
	}
	grps.max = function (i) {
		if (this[i].isSorted) {
			return this[i][this[i].length - 1]
		}
		return Math.max.apply(null, this[i])
	}
	grps.isEmpty = function (i) {
		return this[i].length === 0
	}
	grps.customSort = function (i) {
		this[i].sort((a, b) => a - b)
		this[i].isSorted = true
	}
	grps.biggestGap = function (i) {
		if (!this[i].isSorted) {
			this.customSort(i)
		}

		let size = -1
		let index = -1
		for (let j = 0; j < this[i].length - 1; j++) {
			let tmp = this[i][j + 1] - this[i][j]
			if (tmp > size) {
				size = tmp
				index = j
			}
		}
		return { size, index, grpIndex: i }
	}

	let lfg = 0 // lastly filled group

	for (let i = 0; i < data.length; i++) {
		let n = data[i]
		if (lfg === grps.length - 1) {
			grps[lfg].push(n)
			//grps.sort((a, b) => a - b)
			continue
		}

		let leftDelta = n - grps.min(lfg)
		let rightDelta
		if (typeof data[i + 1] === 'undefined') {
			rightDelta = grps.max(grps.length - 1) - n
		} else {
			rightDelta = data[i + 1] - n
		}
		if (leftDelta <= rightDelta) {
			grps[lfg].push(n)
		} else {
			grps[lfg + 1].push(n)
			lfg += 1
		}
	}

	let emptyGrps = 0
	for (let i = grps.length - 1; i >= 0; i--) {
		if (grps[i].length === 0) {
			emptyGrps += 1
			grps.splice(i, 1)
		}
	}

	let repeat = true
	while (repeat) {
		repeat = false

		let gaps = []
		for (let i = 0; i < grps.length; i++) {
			gaps.push(grps.biggestGap(i))
		}
		gaps.sort((a, b) => b.size - a.size)
		gaps.splice(emptyGrps)
		gaps.sort((a, b) => a.grpIndex - b.grpIndex)

		// TODO add isSorted
		let noLeft = true
		for (let i = 0; i < gaps.length; i++) {
			let gap = gaps[i]
			if (gap.size === -1) {
				repeat = true
			} else {
				let grp = grps[gap.grpIndex]
				let rightGrp = grp.splice(gap.index + 1, grp.length - gap.index - 1)
				grps.splice(gap.grpIndex + 1, 0, rightGrp)
				grps[gap.grpIndex + 1].isSorted = grps[gap.grpIndex + 1].length === 1
				for (let j = i + 1; j < gaps.length; j++) {
					gaps[j].grpIndex += 1
				}
				emptyGrps -= 1
				noLeft = false
			}
		}
		if (noLeft) {
			repeat = false
		}
	}

	if (emptyGrps === 0) {
		repeat = true
		while (repeat) {
			repeat = false
			for (let i = 0; i < grps.length; i++) {
				if (grps[i].length === 1) {
					continue
				}
				if (i !== 0) {
					let toArrange = grps.min(i)
					let left = toArrange - grps.min(i - 1)
					let right = grps.max(i) - toArrange
					if (right > left || (right === left && grps[i].length - grps[i - 1].length > 1)) {
						grps[i].shift()
						grps[i - 1].push(toArrange)
						repeat = true
						break
					}
				}
				if (i !== grps.length - 1) {
					let toArrange = grps.max(i)
					let left = toArrange - grps.min(i)
					let right = grps.max(i + 1) - toArrange
					if (left > right || (left === right && grps[i].length - grps[i + 1].length > 1)) {
						grps[i].pop()
						grps[i + 1].unshift(toArrange)
						repeat = true
						break
					}
				}
			}
		}
	}

	return { groups: grps, empty: emptyGrps }
}
