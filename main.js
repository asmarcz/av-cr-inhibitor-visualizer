let v = document.getElementById('visualization')

function isUpperCase(str) {
	return str === str.toUpperCase()
}

function isLowerCase(str) {
	return str === str.toLowerCase()
}

function isSameCase(one, two) {
	return (isUpperCase(one) && isUpperCase(one)) || (isLowerCase(one) && isLowerCase(two))
}

class InfoBox {
	constructor() {
		this.template = document.getElementById('info-box-template').innerHTML.trim()
		this.element = document.getElementById('info-box')
		this.closeButton = this.element.children[0]
		this.description = this.element.children[1]
		this.previous = this.element

		this.closeButton.addEventListener('click', () => this.hide())
		document.addEventListener('keydown', ev => {
			if (ev.key === 'Escape') {
				this.hide()
			}
		})

		this.colorMap = document.getElementById('color-map')
		this.colorMap.originalHeight = this.colorMap.style.maxHeight
	}

	show(el) {
		if (el.tagName === 'text') {
			el = el.previousElementSibling
		}
		let template = this.template
		for (let key in el.dataset) {
			template = template.replace(`{{${key}}}`, el.dataset[key])
		}
		this.description.innerHTML = template

		this.previous.classList.remove('highlighted')
		el.classList.add('highlighted')
		this.previous = el

		this.element.classList.remove('d-none')

		this.colorMap.style.maxHeight = `calc(99vh - 2.5em - ${this.element.clientHeight}px)`
	}

	hide() {
		this.element.classList.add('d-none')
		this.previous.classList.remove('highlighted')

		this.colorMap.style.maxHeight = this.colorMap.originalHeight
	}
}

class SettingsBox {
	constructor(positive, negative) {
		this.box = document.getElementById('settings-box')
		this.colorMap = document.getElementById('color-map')

		this.splitCheckbox = document.getElementById('negative-positive')
		this.isSplit = !this.splitCheckbox.checked

		this.colorMapBorder = 2
		this.longestNumber = 0
		for (let i = 0; i < data.length; i++) {
			this.longestNumber = Math.max(this.longestNumber, data[i].energy.toString().split('.')[0].length)
		}

		this.positiveMax = positive
		this.negativeMax = negative
		this.blocks = document.getElementsByClassName('range');
		for (let i = 0; i < this.blocks.length; i++) {
			for (let input of this.blocks[i].children) {
				input.addEventListener('input', (e) => this.handleChangeRange(i, parseInt(e.target.value)))
			}
		}
		this.positiveNumber = Math.max(1, Math.floor(positive / 2))
		this.negativeNumber = Math.max(1, Math.floor(negative / 2))
		this.handleChangeRange(0, this.positiveNumber)
		this.handleChangeRange(1, this.negativeNumber)
		this.setMax(0, positive)
		this.setMax(1, negative)
		this.handleSplitChange()
	}

	get max() {
		return this.positiveMax + this.negativeMax
	}

	handleSplitChange() {
		this.isSplit = !this.isSplit

		if (this.isSplit) {
			this.blocks[1].classList.add('d-flex')
			this.blocks[1].classList.remove('d-none')
			this.setMax(0, this.positiveMax)
			this.setMax(1, this.negativeMax)
		} else {
			this.blocks[1].classList.add('d-none')
			this.blocks[1].classList.remove('d-flex')
			this.setMax(0, this.max)
		}

		this.recomputeGroups()
	}

	handleChangeRange(i, val) {
		if (i === 0) {
			this.positiveNumber = val
		} else {
			this.negativeNumber = val
		}
		this.blocks[i].children[0].value = val
		this.blocks[i].children[1].value = val

		this.recomputeGroups()
	}

	setMax(i, val) {
		for (let input of this.blocks[i].children) {
			input.max = val
			input.value = Math.min(parseInt(input.value), val)
		}
	}

	recomputeGroups() {
		let branches = document.getElementById('branches')
		if (branches === null) {
			return
		}
		function changeColors(arr, pallete) {
			for (let i = 0; i < arr.length; i++) {
				for (let j = 0; j < arr[i].length; j++) {
					for (let k = 0; k < branches.children.length; k++) {
						const element = branches.children[k];
						if (parseFloat(element.dataset.energy) === arr[i][j]) {
							element.setAttribute('fill', pallete[i])
						}
					}
				}
			}
		}

		this.fillMap = (arr, pallete) => {
			for (let i = 0; i < arr.length; i++) {
				let block = document.createElement('div')
				block.style.backgroundColor = pallete[i]
				let text = document.createElement('span')
				text.title = arr[i][0] + '\n' + arr[i][arr[i].length - 1]
				/*let first = arr[i][0].toFixed(4)
				first = '&nbsp'.repeat(this.longestNumber - first.toString().split('.')[0].length) + first
				let second = arr[i][arr[i].length - 1].toFixed(4)
				second = '&nbsp'.repeat(this.longestNumber - second.toString().split('.')[0].length) + second
				text.innerHTML = first + '<br>' + second*/
				let num = ((arr[i][0] + arr[i][arr[i].length - 1]) / 2).toFixed(2)
				text.innerHTML = '&nbsp'.repeat(this.longestNumber - num.toString().split('.')[0].length) + num
				let blockWrap = document.createElement('div')
				blockWrap.appendChild(block)
				blockWrap.appendChild(text)
				this.colorMap.appendChild(blockWrap)
			}
		}

		this.colorMap.innerHTML = ''
		if (this.isSplit) {
			negatives.push(0)
			positives.unshift(0)

			let neg = group(negatives, this.negativeNumber).groups
			let pos = group(positives, this.positiveNumber).groups

			let negativePallete = chroma.scale(
				colorPallete.slice(0, colorPallete.zeroIndex + 1)
			).colors(this.negativeNumber)
			let positivePallete = chroma.scale(
				colorPallete.slice(colorPallete.zeroIndex + 1)
			).colors(this.positiveNumber)

			changeColors(neg, negativePallete)
			changeColors(pos, positivePallete)

			negatives.pop()
			positives.shift()

			this.fillMap(neg, negativePallete)
			this.fillMap(pos, positivePallete)
		} else {
			let concatenated = negatives.concat([0]).concat(positives)
			let all = group(concatenated, this.positiveNumber).groups
			let zeroIndex = all.findIndex((el) => el.includes(0))

			let neg = all.slice(0, zeroIndex + 1)
			let pos = all.slice(zeroIndex + 1)

			let negativePallete = chroma.scale(
				colorPallete.slice(0, colorPallete.zeroIndex + 1)
			).colors(zeroIndex + 1)
			let positivePallete = chroma.scale(
				colorPallete.slice(colorPallete.zeroIndex + 2) // skip second zero color
			).colors(all.length - zeroIndex - 1)

			changeColors(neg, negativePallete)
			changeColors(pos, positivePallete)

			this.fillMap(neg, negativePallete)
			this.fillMap(pos, positivePallete)
		}
	}

	hide() {
		this.box.classList.add('hide-settings')
		this.box.onclick = () => {
			this.show()
		}
	}

	show() {
		this.box.classList.remove('hide-settings')
		this.box.onclick = ''
	}
}

function init(data) {
	data.sort(function (a, b) {
		if (a.selection === b.selection) {
			if (isUpperCase(a.acid)) {
				return -1
			}
			return 1
		} else if (a.selection < b.selection) {
			return -1
		}
		return 1
	})
	console.log(data)

	let INHIBITOR_DIAMETER = 1000
	let DISTANCE_SCALE = 10

	let maxEnergy = -Infinity
	let minEnergy = Infinity
	let maxDistance = -Infinity
	let minDistance = Infinity
	for (let i = 0; i < data.length; i++) {
		maxEnergy = Math.max(data[i].energy, maxEnergy)
		minEnergy = Math.min(data[i].energy, minEnergy)
		maxDistance = Math.max(data[i].distance, maxDistance)
		minDistance = Math.min(data[i].distance, minDistance)
	}

	let ends = []
	let branches = document.createElementNS('http://www.w3.org/2000/svg', 'g')
	branches.setAttribute('id', 'branches')

	function createLetter(i) {
		let letter = document.createElementNS('http://www.w3.org/2000/svg', 'text')
		letter.style.fontSize = fontSize + 'px'
		letter.style.fontFamily = 'monospace'
		letter.textContent = abbreviations[data[i].acid.toLowerCase()]
		return letter
	}

	let w = 40
	let endScale = 3.5
	let letters = []
	let fontSize = endScale * w
	for (let i = 0, rotation = 360 / data.length; i < data.length; i++) {
		let currentRotation = rotation * i
		let line = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
		let h = (((data[i].distance - minDistance) / (maxDistance - minDistance) * 100)) * DISTANCE_SCALE + INHIBITOR_DIAMETER / 1.5
		line.setAttribute('height', h)
		line.setAttribute('width', w)
		line.setAttribute('transform', `rotate(${currentRotation}) translate(-${w / 2})`)
		line.dataset.energy = data[i].energy
		branches.appendChild(line)
		let a
		let letter = createLetter(i)
		let yHelper
		if (data[i].acid === data[i].acid.toLowerCase()) {
			a = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
			a.setAttribute('height', endScale * w)
			a.setAttribute('width', endScale * w)
			a.setAttribute('x', - endScale / 2 * w)
			a.setAttribute('y', h)
			a.setAttribute('transform', `rotate(${currentRotation})`)
			yHelper = h
		} else {
			a = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
			let r = endScale * w / 2
			a.setAttribute('r', r)
			a.setAttribute('cy', h + r / 2)
			a.setAttribute('transform', `rotate(${currentRotation})`)
			yHelper = h - r / 2
		}
		a.setAttribute('fill', 'white')
		a.setAttribute('stroke', 'black')
		a.setAttribute('stroke-width', '4')
		Object.assign(a.dataset, data[i])
		a.setAttribute('onmouseover', 'infoBox.show(this)')
		letter.setAttribute('onmouseover', 'infoBox.show(this)')
		ends.push(a)
		{
			let r = endScale * w
			let rr = r + yHelper - fontSize / 2
			let x = - rr * Math.sin(currentRotation * Math.PI / 180)
			let y = rr * Math.cos(currentRotation * Math.PI / 180)
			letter.setAttribute('x', x - w)
			letter.setAttribute('y', y + 1.3 * w)
		}
		letters.push(letter)
	}


	let strokeWidth = 15
	v.appendChild(branches)
	v.appendChild(ends[0])
	let previousBCR = ends[0].getBoundingClientRect()
	v.removeChild(ends[0])
	for (let i = 1; i < data.length; i++) {
		v.appendChild(ends[i])

		let currentBCR = ends[i].getBoundingClientRect()
		if (data[i - 1].selection === data[i].selection - 1 || data[i - 1].selection === data[i].selection) {
			let connector = document.createElementNS('http://www.w3.org/2000/svg', 'line')
			connector.setAttribute('stroke', 'black')
			connector.setAttribute('stroke-width', strokeWidth)
			connector.setAttribute('x1', previousBCR.left + previousBCR.width / 2 - strokeWidth / 2)
			connector.setAttribute('y1', previousBCR.top + previousBCR.height / 2 - strokeWidth / 2)
			connector.setAttribute('x2', currentBCR.left + currentBCR.width / 2 - strokeWidth / 2)
			connector.setAttribute('y2', currentBCR.top + currentBCR.height / 2 - strokeWidth / 2)
			v.appendChild(connector)
		}

		previousBCR = currentBCR
		v.removeChild(ends[i])
	}
	for (let i = 0; i < ends.length; i++) {
		v.appendChild(ends[i])
		v.appendChild(letters[i])
	}

	let inhibitor = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
	inhibitor.setAttribute('r', INHIBITOR_DIAMETER / 2)
	inhibitor.setAttribute('fill', 'white')
	inhibitor.setAttribute('stroke', 'black')
	inhibitor.setAttribute('stroke-width', strokeWidth * 2)
	let inhibitorTemplate = document.getElementById('inhibitor-template').innerHTML.trim()
	let inhibitorDict = {
		width: INHIBITOR_DIAMETER,
		height: INHIBITOR_DIAMETER,
		xOffset: -INHIBITOR_DIAMETER / 2,
		yOffset: -INHIBITOR_DIAMETER / 2,
		src: 'assets/figure.png',
	}
	for (let key in inhibitorDict) {
		inhibitorTemplate = inhibitorTemplate.replace(`{{${key}}}`, inhibitorDict[key])
	}
	v.appendChild(inhibitor)
	v.innerHTML += inhibitorTemplate.replace(/[\t\n]/g, '')
	if (platform.layout === 'WebKit') {
		v.getElementById('figure').parentElement.remove()
	}

	panZoom = svgPanZoom('#visualization', { minZoom: -Infinity, maxZoom: Infinity })
	function resized() {
		panZoom.resize()
		panZoom.fit()
		panZoom.center()
	}
	window.addEventListener('resize', resized)
	window.addEventListener('load', resized)

	document.querySelectorAll('[v-hidden]')
		.forEach(function (el) {
			el.removeAttribute('v-hidden')
		})
}

/*fetch('energies').then(res => res.json())
	.then(init)*/

let positives = []
let negatives = []
let data = [{ "acid": "ala", "energy": "-0.402860", "selection": "16", "distance": "4.587" }, { "acid": "glu", "energy": "0.455430", "selection": "165", "distance": "4.668" }, { "acid": "val", "energy": "1.892670", "selection": "168", "distance": "4.235" }, { "acid": "asn", "energy": "0.368200", "selection": "221", "distance": "3.526" }, { "acid": "asn", "energy": "2.487560", "selection": "222", "distance": "4.158" }, { "acid": "asn", "energy": "1.864060", "selection": "289", "distance": "4.619" }, { "acid": "tyr", "energy": "0.504940", "selection": "291", "distance": "4.635" }, { "acid": "trp", "energy": "1.838830", "selection": "393", "distance": "4.527" }, { "acid": "glu", "energy": "-0.650170", "selection": "400", "distance": "3.646" }, { "acid": "trp", "energy": "-0.525740", "selection": "401", "distance": "4.689" }, { "acid": "ALA", "energy": "-0.230360", "selection": "16", "distance": "3.811" }, { "acid": "GLN", "energy": "-12.291010", "selection": "19", "distance": "1.833" }, { "acid": "ARG", "energy": "34.661510", "selection": "76", "distance": "3.590" }, { "acid": "HIE", "energy": "-3.586100", "selection": "120", "distance": "1.910" }, { "acid": "TRP", "energy": "-3.724200", "selection": "121", "distance": "2.264" }, { "acid": "ASN", "energy": "-4.003500", "selection": "164", "distance": "1.931" }, { "acid": "GLU", "energy": "-100.505970", "selection": "165", "distance": "1.556" }, { "acid": "TRP", "energy": "-0.976020", "selection": "167", "distance": "4.897" }, { "acid": "VAL", "energy": "-2.160160", "selection": "168", "distance": "2.325" }, { "acid": "VAL", "energy": "-0.544650", "selection": "172", "distance": "3.826" }, { "acid": "HIE", "energy": "0.855130", "selection": "179", "distance": "1.945" }, { "acid": "ASN", "energy": "0.075130", "selection": "221", "distance": "2.532" }, { "acid": "ASN", "energy": "3.961510", "selection": "289", "distance": "3.353" }, { "acid": "TYR", "energy": "-7.351010", "selection": "291", "distance": "2.647" }, { "acid": "MET", "energy": "-0.667330", "selection": "317", "distance": "2.924" }, { "acid": "TRP", "energy": "-5.726440", "selection": "319", "distance": "2.162" }, { "acid": "GLU", "energy": "-90.904620", "selection": "346", "distance": "1.915" }, { "acid": "TRP", "energy": "-9.111650", "selection": "393", "distance": "2.658" }, { "acid": "ASN", "energy": "-0.157300", "selection": "398", "distance": "3.682" }, { "acid": "GLU", "energy": "-82.701560", "selection": "400", "distance": "1.682" }, { "acid": "TRP", "energy": "-3.134770", "selection": "401", "distance": "1.869" }, { "acid": "PHE", "energy": "-6.006850", "selection": "409", "distance": "2.659" }]
for (let i = 0; i < data.length; i++) {
	data[i].energy = parseFloat(data[i].energy)
	data[i].selection = parseInt(data[i].selection)
	data[i].distance = parseFloat(data[i].distance)
	if (data[i].energy >= 0) {
		positives.push(data[i].energy)
	} else {
		negatives.push(data[i].energy)
	}
}
[positives, negatives].forEach(ar => ar.sort((a, b) => a - b))

let infoBox = new InfoBox()

let panZoom
init(data)

let settingsBox = new SettingsBox(positives.length, negatives.length)
